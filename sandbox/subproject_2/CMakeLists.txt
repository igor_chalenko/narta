cmake_minimum_required(VERSION 3.19)
project(subproject_2)

cmake_policy(SET CMP0118 OLD)

set_property(GLOBAL PROPERTY CMAKE_C_KNOWN_FEATURES xxx)
get_property(_xxx GLOBAL PROPERTY CMAKE_C_KNOWN_FEATURES)
message(STATUS "_xxx = ${_xxx}")

#set_property(CACHE non_existing PROPERTY VALUE value)

get_property(_labels SOURCE src/foo.cpp PROPERTY LABELS)
message(STATUS "1. labels = ${_labels}")
get_property(_labels SOURCE ../subproject_1/src/foo.cpp PROPERTY LABELS)
message(STATUS "2. labels = ${_labels}")
get_property(_labels SOURCE ../subproject_1/src/foo.cpp DIRECTORY ../subproject_1 PROPERTY LABELS)
message(STATUS "3. labels = ${_labels}")
get_property(_labels SOURCE src/foo.cpp DIRECTORY ../subproject_1 PROPERTY LABELS)
message(STATUS "4. labels = ${_labels}")
#set_property(SOURCE src/foo1.cpp DIRECTORY ../subproject_1 PROPERTY GENERATED 1)
#get_property(_generated SOURCE ../subproject_1/src/foo.cpp DIRECTORY ../subproject_1 PROPERTY GENERATED)
#message(STATUS "generated = ${_generated}")
#get_property(_generated SOURCE src/foo1.cpp DIRECTORY ../subproject_1 PROPERTY GENERATED)
#message(STATUS "generated = ${_generated}")

#get_property(_cache_entries GLOBAL PROPERTY CACHE_VARIABLES)
#message(STATUS "[GLOBAL] cache_entries = ${_cache_entries}")
#get_property(_cache_entries DIRECTORY PROPERTY CACHE_VARIABLES)
#message(STATUS "[DIRECTORY] cache_entries = ${_cache_entries}")
