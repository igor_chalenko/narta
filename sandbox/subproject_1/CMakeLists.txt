cmake_minimum_required(VERSION 3.19)
project(subproject_1)
cmake_policy(SET CMP0118 OLD)

set(CMAKE_SOURCE_DIR  xxx)
set_property(SOURCE $<x:src/foo.cpp> PROPERTY LABELS "cool label")
get_property(_labels SOURCE $<x:src/foo.cpp> PROPERTY LABELS)
message(STATUS "0. labels = ${_labels}")
#set(var value CACHE STRING "help")
#set_property(CACHE var PROPERTY VALUE value)
#get_property(_cache_entries GLOBAL PROPERTY CACHE_VARIABLES)
#message(STATUS "[GLOBAL] cache_entries = ${_cache_entries}")
#get_property(_cache_entries DIRECTORY PROPERTY CACHE_VARIABLES)
#message(STATUS "[DIRECTORY] cache_entries = ${_cache_entries}")
