include(${CMAKE_CURRENT_LIST_DIR}/Globals.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Sources.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Targets.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/CacheVariables.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Variables.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Tests.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Directories.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Export.cmake)


