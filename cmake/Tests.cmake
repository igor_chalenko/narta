include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

function(narta_test_properties directory test callback)
    set(_properties ${NARTA_CMAKE_PROPERTIES})

    foreach (_name_template IN LISTS _properties)
        _narta_materialize_variable(${_name_template} _property_names)
        foreach (_property IN LISTS _property_names)
            get_test_property(${test} ${_property} _value)
            if (NOT "${_value}" STREQUAL "" AND NOT _value STREQUAL "NOTFOUND")
                cmake_language(EVAL CODE "${callback}(\"${directory}\" \"${_test}\" ${_property} \"${_value}\")")
            endif ()
        endforeach ()
    endforeach ()
endfunction()

function(narta_get_test directory test_name)
    if (ARGC EQUAL 3)
        set(callback ${ARGV2})
    else()
        set(callback test_callback)
    endif()

    get_property(_tests DIRECTORY "${directory}" PROPERTY TESTS)
    #message(STATUS "[narta_get_test] Tests in ${directory}: ${_tests}")
    if (test_name IN_LIST _tests)
        #message(STATUS "[narta_get_test] Calling ${callback}(${test_name})...")
        cmake_language(EVAL CODE "${callback}(\"${test_name}\")")
    endif()
endfunction()

function(narta_get_test_property directory test_name property_name callback)
    get_property(_tests DIRECTORY "${directory}" PROPERTY TESTS)
    if ("${test_name}" IN_LIST _tests)
        get_property(_value TEST "${test_name}" PROPERTY ${property_name})
        cmake_language(EVAL CODE "${callback}(\"${_directory}\" \"${test_name}\" ${property_name} \"${_value}\")")
    endif()
endfunction()
