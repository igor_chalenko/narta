include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

function(narta_cache_variables callback property_callback)
    get_cmake_property(_variables CACHE_VARIABLES)
    list(REMOVE_DUPLICATES _variables)
    list(SORT _variables)

    foreach(_var IN LISTS _variables)
        get_property(_helpstring CACHE "${_var}" PROPERTY HELPSTRING)
        get_property(_type CACHE "${_var}" PROPERTY TYPE)
        get_property(_value CACHE "${_var}" PROPERTY VALUE)
        string(REPLACE "\\" "\\\\" _value "${_value}")
        string(REPLACE "\"" "\\\"" _value "${_value}")
        set(_code "${callback}(\"${_var}\" \"${_value}\" \"${_type}\" \"${_helpstring}\")")
        cmake_language(EVAL CODE "${_code}")
        narta_cache_variable_properties(${_var} ${property_callback})
    endforeach()
endfunction()

function(narta_cache_variable_properties _var property_callback)
    set(_properties
            ADVANCED
            HELPSTRING
            MODIFIED
            STRINGS
            TYPE
            VALUE
            )

    unset(_props)
    foreach (_name_template IN LISTS _properties)
        _narta_materialize_variable(${_name_template} _property_names)
        foreach (_property IN LISTS _property_names)
            get_property(_value CACHE ${_var} PROPERTY ${_property})
            if (NOT _value STREQUAL "${_var}-NOTFOUND")
                string(REPLACE "\\" "\\\\" _value "${_value}")
                string(REPLACE "\"" "\\\"" _value "${_value}")
                set(_code "${property_callback}(\"${_var}\" \"${_property}\" \"${_value}\")")
                cmake_language(EVAL CODE "${_code}")
            endif ()
        endforeach ()
    endforeach ()
endfunction()

function(narta_get_cache_variable _var)
    if (ARGC EQUAL 2)
        set(callback ${ARGV1})
    else()
        set(callback cache_variable_callback)
    endif()

    get_cmake_property(_variables CACHE_VARIABLES)
    if (_var IN_LIST _variables)
        get_property(_value CACHE ${_var} PROPERTY VALUE)
        get_property(_type CACHE ${_var} PROPERTY TYPE)
        get_property(_helpstring CACHE ${_var} PROPERTY HELPSTRING)

        string(REPLACE "\\" "\\\\" _value "${_value}")
        string(REPLACE "\"" "\\\"" _value "${_value}")

        string(REPLACE "\\" "\\\\" _helpstring "${_helpstring}")
        string(REPLACE "\"" "\\\"" _helpstring "${_helpstring}")

        # message(STATUS "calling cache variable callback")
        cmake_language(EVAL CODE "${callback}(\"${_var}\" \"${_value}\" \"${_type}\" \"${_helpstring}\")")
    endif ()
endfunction()

function(narta_get_cache_variable_property _var _property)
    if (ARGC EQUAL 3)
        set(callback ${ARGV2})
    else()
        set(callback cache_variable_property_callback)
    endif()

    get_property(_value CACHE ${_var} PROPERTY ${_property})
    if (NOT _value STREQUAL "${_var}-NOTFOUND")
        cmake_language(EVAL CODE "${callback}(\"${_var}\" \"${_property}\" \"${_value}\")")
    endif ()
endfunction()
