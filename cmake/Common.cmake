function(run_test _function _invocation_count)
    set_property(GLOBAL PROPERTY _invoked 0)
    cmake_language(EVAL CODE "${_function}()")
    get_property(_invoked GLOBAL PROPERTY _invoked)
    assertEqual(${_invoked} ${_invocation_count})
endfunction()

function(increment_invocation_count)
    get_property(_invoked GLOBAL PROPERTY _invoked)
    math(EXPR _invoked "${_invoked} + 1")
    set_property(GLOBAL PROPERTY _invoked ${_invoked})
endfunction()

function(_narta_set_property _keyword_1 _target _keyword_2 _property _value)
    set_property(TARGET ${_target} PROPERTY ${_property} "${_value}")
endfunction()

macro(_narta_get_property _out _keyword_1 _target _keyword_2 _property)
    get_target_property(${_out} ${_target} ${_property})
endmacro()

function(_narta_materialize_variable _name_template _out_list)
    unset(_result)
    if (${_name_template} MATCHES "<CONFIG>")
        if (DEFINED CMAKE_CONFIGURATION_TYPES)
            foreach(_config IN LISTS CMAKE_CONFIGURATION_TYPES)
                string(REPLACE "<CONFIG>" "${_config}" _name ${_name_template})
                list(APPEND _result ${_name})
            endforeach()
        else()
            string(REPLACE "<CONFIG>" "${CMAKE_BUILD_TYPE}" _name ${_name_template})
            list(APPEND _result ${_name})
        endif()
    endif()
    get_property(_enabled_languages GLOBAL PROPERTY ENABLED_LANGUAGES)
    if (${_name_template} MATCHES "<LANG>")
        foreach(_language IN LISTS _enabled_languages)
            string(REPLACE "<LANG>" "${_language}" _name ${_name_template})
            list(APPEND _result ${_name})
        endforeach()
    endif()

    # todo what else?
    if (${_name_template} MATCHES "<tagname>")
        set(_tags AutoGen Link DependentUpon DesignTime SubType)
        foreach(_tag IN LISTS _tags)
            string(REPLACE "<tagname>" "${_tag}" _name ${_name_template})
            list(APPEND _result ${_name})
        endforeach()
    endif()

    # this will leave <tagname> un-replaced, must be handled by a subsequent call
    # for an individual target;
    # variables such as `*_<variable>`, *_<tool>, *_<an-attribute>, *_<refname>, *_<section>
    # will have to be provided manually - we don't know them
    if (${_name_template} MATCHES "<tagname>")
        set(_xcode_embed_types FRAMEWORKS APP_EXTENSIONS PLUGINS)
        foreach(_type IN LISTS _xcode_embed_types)
            string(REPLACE "<type>" "${_type} " _name ${_name_template})
            list(APPEND _result ${_name})
        endforeach()
    endif()
    if (NOT _result)
        set(_result ${_name_template})
    endif()
    set(${_out_list} ${_result} PARENT_SCOPE)
endfunction()

macro(_narta_supported_cmake_properties _out)
    ## Get all properties that cmake supports
    execute_process(COMMAND ${CMAKE_COMMAND} --help-property-list OUTPUT_VARIABLE _properties)
    ## Convert command output into a CMake list
    string(REGEX REPLACE ";" " \\\\;" _properties " ${_properties}")
    string(REGEX REPLACE "\n" ";" _properties " ${_properties}")
    list(REMOVE_DUPLICATES _properties)
    set(${_out} ${_properties})
endmacro()

_narta_supported_cmake_properties(NARTA_CMAKE_PROPERTIES)