set(_ignorable_variables
        ARGV
        ARGV0
        ARGV1
        ARGV2
        _snapshot
        _callback
        _out
        _current_snapshot
        _prev_snapshot
        )

# BINARY_DIR is given to add_directory
set(_ignorable_directory_properties
        LISTFILE_STACK
        VARIABLES
        TESTS
        CACHE_VARIABLES
        BINARY_DIR
        PARENT_DIRECTORY
        SUBDIRECTORIES)
set(_ignorable_global_properties CACHE_VARIABLES)
set(_ignorable_target_properties IMPORTED_GLOBAL)

function(export_message _message)
    get_property(_out GLOBAL PROPERTY _out)
    string(APPEND _out "${_message}\n")
    set_property( GLOBAL PROPERTY _out "${_out}")
endfunction()

function(global_property_callback _property )
    string(REPLACE "\"" "\\\"" _value "${_value}")
    string(REPLACE "\\" "\\\\" _value "${_value}")
    string(REPLACE "'''" "\\'\\'\\'" _value "${_value}")
    if (NOT _property IN_LIST _ignorable_global_properties)
        if (_operation STREQUAL REMOVE)
            set(_fun remove_global_property)
        else()
            set(_fun set_global_property)
        endif()
        if (ARGC EQUAL 2)
            set(_value ${ARGV1})
            export_message("  - \"${_fun}('''${_property}''', '''${_value}''')\"")
        else()
            export_message("  - \"${_fun}('''${_property}''', None)\"")
        endif()
    endif()
endfunction()

function(variable_callback _var)
    if (NOT _var IN_LIST _ignorable_variables)
        if (ARGC EQUAL 2)
            set(_value ${ARGV1})
            string(REPLACE "\\" "\\\\" _value "${_value}")
            string(REPLACE "\"" "\\\"" _value "${_value}")
            export_message("  - \"set('''${_var}''', '''${_value}''')\"")
        else()
            export_message("  - \"set('''${_var}''', None)\"")
        endif()
    endif()
endfunction()

function(cache_variable_callback name value type helpstring)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")

    export_message("  - \"set_cache_variable('''${name}''', '''${value}''', '''${type}''', '''${helpstring}''')\"")
endfunction()

function(cache_variable_property_callback name property value)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")
    export_message("  - \"set_cache_variable_property('''${name}''', '''${property}''', '''${value}''')\"")
endfunction()

function(target_property_callback target property value)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")
    export_message("  - \"set_target_property('''${target}''', '''${property}''', '''${value}''')\"")
endfunction()

function(test_property_callback directory test property value)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")
    export_message("  - \"set_test_property('''${test}''', '''${property}''', '''${value}''')\"")
endfunction()

function(source_file_property_callback directory source_file property value)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")
    export_message("  - \"set_source_property('''${directory}''', '''${source_file}''', '''${property}''', '''${value}''')\"")
endfunction()

function(directory_property_callback directory property value)
    string(REPLACE "\\" "\\\\" value "${value}")
    string(REPLACE "\"" "\\\"" value "${value}")
    if (NOT property IN_LIST _ignorable_directory_properties)
        export_message("  - \"set_directory_property('''${directory}''', '''${property}''', '''${value}''')\"")
    endif()
endfunction()

function(target_callback directory target kind)
    get_property(_imported TARGET ${target} PROPERTY IMPORTED)
    if (kind STREQUAL ALIAS)
        get_property(_aliased_target TARGET ${target} PROPERTY ALIASED_TARGET)
        export_message("  - \"add_alias_target('''${target}''', '''${_aliased_target}''')\"")
    elseif(_imported)
        export_message("  - \"add_imported_library('''${target}''', '''${kind}''')\"")
    else()
        string(TOLOWER ${kind} _kind_lower)
        set(_fun add_${_kind_lower})
        export_message("  - \"${_fun}('''${target}''')\"")
    endif()
endfunction()

function(test_callback test)
    export_message("  - \"add_test('''${test}''')\"")
endfunction()

function(directory_callback source_dir binary_dir parent_dir)
    message(STATUS "[directory_callback] ${_operation} ${_kind} ${_scope} ${ARGN}")
    export_message("  - \"add_directory('''${source_dir}''', '''${binary_dir}''', '''${parent_dir}''')\"")
endfunction()

function(export_initial_state)
    set_property(GLOBAL PROPERTY _out "")
    narta_globals(global_property_callback)
    narta_variables(variable_callback)
    narta_cache_variables(cache_variable_callback cache_variable_property_callback)

    export_message("  - \"add_directory('''${CMAKE_SOURCE_DIR}''', '''${CMAKE_BINARY_DIR}''')\"")
endfunction()

