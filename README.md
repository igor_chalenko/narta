# Narta - CMake state exporter

Global properties
-----------------


Variables
---------
Parent scope is always the same (global scope), and the unique identifier is the variable name.
Variables don't have any properties, except value - the functionality of `define_property` is
not currently supported.

Both cache and non-cache variables are extracted from `VARIABLES` property of the global scope,
thus it's necessary to additionally query for `CACHE` property of each variable to find out
whether it's a cache variable or not.


Cache variables
---------------
Parent scope is always the same (global scope), and the unique identifier is the variable name.

Sources
-------
Source files are identified by their name.

Directories
-----------
Directories belong to the global scope and are identified by their name.




License
-------

This package is under the MIT license. See the 
=======
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
