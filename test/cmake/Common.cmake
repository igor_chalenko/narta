if (WIN32)
    set(_generator "") # "-G CodeBlocks - MinGW Makefiles")
else()
    set(_generator "")
endif()

if (DEFINED narta.path)
    include(${narta.path}/Globals.cmake)
    include(${narta.path}/Variables.cmake)
    include(${narta.path}/CacheVariables.cmake)
    include(${narta.path}/Sources.cmake)
    include(${narta.path}/Tests.cmake)
    include(${narta.path}/Targets.cmake)
    include(${narta.path}/Directories.cmake)
else()
    message(FATAL_ERROR "`narta.path` must be defined")
endif()

##############################################################################
#.rst:
#
# .. cmake:command:: assert_same
#
# .. code-block:: cmake
#
#    assert_same(value)
#
# If the strings ``str1`` and ``str2`` are not equal, emits an error message.
# Does nothing otherwise.
##############################################################################
macro(assertEqual str1 str2)
    if (NOT "${str1}" STREQUAL "${str2}")
        message(FATAL_ERROR "`${str1}` is not equal to `${str2}`")
    endif ()
endmacro()