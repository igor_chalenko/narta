cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

set(_ignorable LISTFILE_STACK VARIABLES)

function(setup)
    unset(DIRECTORIES_TEST_ENABLED CACHE)
endfunction()

function(get_directory_callback _source_dir _binary_dir)
    assertEqual(${_source_dir} "${CMAKE_CURRENT_SOURCE_DIR}/subproject")
    increment_invocation_count()
endfunction()

function(get_directory_property_callback directory _property _value)
    increment_invocation_count()
endfunction()

function(test_get_directory)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    add_subdirectory(subproject)
    narta_get_directory("${CMAKE_CURRENT_SOURCE_DIR}/subproject" get_directory_callback)
endfunction()

function(test_get_directory_property)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    narta_get_directory_property("${_directory}" PARENT get_directory_property_callback)
endfunction()

function(directory_properties_callback directory _property _value)
    increment_invocation_count()
endfunction()

function(test_directory_properties)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    narta_directory_properties("${_directory}" directory_properties_callback)
endfunction()

setup()

run_test(test_get_directory 1)
run_test(test_get_directory_property 1)
run_test(test_directory_properties 9)
