cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

set(_ignorable ARGV ARGV0 ARGV1 ARGV2
        _snapshot
        _callback
        _current_snapshot
        _prev_snapshot)

function(setup)
    message(STATUS "[${CMAKE_CURRENT_LIST_FILE}] Unset cached TEST_ENABLED")
    unset(TESTS_TEST_ENABLED CACHE)
endfunction()

function(test_add_callback test)
    #message(STATUS "[${_operation}] ${_kind} ${_scope} ${_test}")
    assertEqual(${test} "add_cool_test")
    increment_invocation_count()
endfunction()

function(test_get_test)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    # narta_tests("${_directory}" ADD_PREV)
    add_test(NAME add_cool_test COMMAND ${CMAKE_COMMAND})
    narta_get_test("${_directory}" add_cool_test test_add_callback)
    # narta_tests_delta("${_directory}" ADD_PREV ADD_LAST test_add_callback test_add_callback)
endfunction()

function(test_get_test_property_callback _directory _test _property _value)
    #message(STATUS "[${_operation}] ${_kind} ${_outer_scope} ${_scope} ${_property} ${_value}")
    assertEqual(${_property} LABELS)
    assertEqual(${_value} cool_label)

    increment_invocation_count()
endfunction()

function(test_get_test_property)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    add_test(NAME add_property_cool_test COMMAND ${CMAKE_COMMAND})
    set_property(TEST add_property_cool_test PROPERTY LABELS cool_label)
    narta_get_test_property("${_directory}" add_property_cool_test LABELS test_get_test_property_callback)

    narta_test_properties("${_directory}" add_property_cool_test test_get_test_property_callback)
endfunction()

setup()

run_test(test_get_test 1)
run_test(test_get_test_property 2)
