cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Overrides.cmake)

function(globals_add_callback _property _value)
    assertEqual(${_property} JOB_POOLS)
    assertEqual("${_value}" cool_pool)
    increment_invocation_count()
endfunction()

function(test_get_global_property)
    set_property(GLOBAL PROPERTY JOB_POOLS cool_pool)
    narta_get_global_property(JOB_POOLS globals_add_callback)
endfunction()

set(_recognized CACHE_VARIABLES CMAKE_ROLE GENERATOR_IS_MULTI_CONFIG IN_TRY_COMPILE MACROS)
function(test_globals_callback property)
    message(STATUS "${property}=${value}")
    if (ARGC EQUAL 2)
        if (property IN_LIST _recognized)
            increment_invocation_count()
        endif()
    endif()
endfunction()

function(test_globals)
    narta_globals(test_globals_callback)
endfunction()

run_test(test_get_global_property 1)
run_test(test_globals 5)
