cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

set(_ignorable ARGV ARGV0 ARGV1 ARGV2
        _snapshot
        _callback
        _current_snapshot
        _prev_snapshot)

function(setup)
    # message(STATUS "[${CMAKE_CURRENT_LIST_FILE}] Unset cached TEST_ENABLED")
    unset(TEST_ENABLED CACHE)
endfunction()

function(source_property_callback directory source_file property value)
    message(STATUS "[source_property_callback] ${directory} ${source_file} ${_property} -> ${_value}")
    assertEqual("${source_file}" "${CMAKE_CURRENT_LIST_DIR}/../test/src/foo.cpp")
    increment_invocation_count()
endfunction()

function(test_get_source_file_property)
    set(_source_file ${CMAKE_CURRENT_LIST_DIR}/../test/src/foo.cpp)
    add_library(foo ${_source_file})
    get_property(_source_dir TARGET foo PROPERTY SOURCE_DIR)
    narta_get_source_file_property(
            "${_source_dir}"
            "${_source_file}"
            LOCATION source_property_callback)
endfunction()

setup()

run_test(test_get_source_file_property 1)

