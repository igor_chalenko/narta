cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

function(test_context_callback _operation _kind _scope)
    if (_kind STREQUAL DIRECTORY)
        message("    ${_scope}:")
    endif()
endfunction()

function(test_context)
    message(STATUS "CMAKE_CURRENT_SOURCE_DIR = ${CMAKE_CURRENT_LIST_DIR}")
    set(_directory "${CMAKE_CURRENT_LIST_DIR}../project_1")
    narta_directories(PREV)
    message("  directories:")
    narta_directories_delta(PREV CURRENT test_context_callback)
endfunction()

test_context()
