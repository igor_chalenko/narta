cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Overrides.cmake)

set(_ignorable ARGV ARGV0 ARGV1 ARGV2
        _snapshot
        _callback
        _current_snapshot
        _prev_snapshot)

function(cache_callback _var _value _type _helpstring)
    assertEqual(${_var} cool_variable)

    increment_invocation_count()
endfunction()

function(cache_variables_callback _var _value _type _helpstring)
    message(STATUS "${_var}=${_value}")
    increment_invocation_count()
endfunction()

function(cache_variable_property_callback _var _property _value)
    if (_value STREQUAL "" OR _value STREQUAL "ON")
        assertEqual(${_property} ADVANCED)

        increment_invocation_count()
    endif()
endfunction()

function(cache_property_callback_2 _var _property _value)
endfunction()

function(test_cache_variable)
    unset(cool_variable CACHE)
    narta_get_cache_variable(cool_variable cache_callback)
    set(cool_variable cool_value CACHE STRING "docs" FORCE)
    narta_get_cache_variable(cool_variable cache_callback)
endfunction()

function(test_cache_variables)
    narta_cache_variables(cache_variables_callback cache_property_callback_2)
endfunction()

function(test_cache_variable_property)
    set(cool_variable cool_value CACHE STRING "docs" FORCE)
    set_property(CACHE cool_variable PROPERTY ADVANCED ON)
    narta_get_cache_variable_property(cool_variable ADVANCED)
    set_property(CACHE cool_variable PROPERTY ADVANCED)
    narta_get_cache_variable_property(cool_variable ADVANCED)
endfunction()

run_test(test_cache_variable 1)
run_test(test_cache_variable_property 2)
run_test(test_cache_variables 6)
