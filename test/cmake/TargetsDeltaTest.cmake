cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)

function(setup)
    unset(TARGETS_TEST_ENABLED CACHE)
endfunction()

function(test_get_target_callback _directory _target _kind)
    message(STATUS "[test_get_target] ${_directory} ${_target}")
    assertEqual(${_directory} "${CMAKE_CURRENT_SOURCE_DIR}")
    assertEqual(${_target} add_cool_library)
    assertEqual(${_kind} INTERFACE_LIBRARY)
    increment_invocation_count()
endfunction()

function(target_get_property_callback target property value)
    message(STATUS "[target_get_property_callback] ${target} ${property} ${value}")
    increment_invocation_count()
endfunction()

function(test_get_target)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    #narta_targets("${_directory}" ADD_PREV)
    add_library(add_cool_library INTERFACE ${CMAKE_CURRENT_LIST_DIR}/../test/src/foo.cpp)
    narta_get_target("${_directory}" add_cool_library test_get_target_callback)
endfunction()

function(test_get_target_property)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    add_library(add_cool_library_2 INTERFACE ${CMAKE_CURRENT_LIST_DIR}/../test/src/foo.cpp)
    narta_get_target_property(add_cool_library_2 NAME target_get_property_callback)
    narta_get_target_property(add_cool_library_2 SOURCE_DIR target_get_property_callback)
endfunction()

function(target_properties_callback target property value)
    message(STATUS "[target_properties_callback] ${target} ${property} ${value}")
    increment_invocation_count()
endfunction()

function(test_narta_target_properties)
    set(_directory "${CMAKE_CURRENT_SOURCE_DIR}")
    add_library(add_cool_library_3 INTERFACE ${CMAKE_CURRENT_LIST_DIR}/../test/src/foo.cpp)
    narta_target_properties("${_directory}" add_cool_library_3 target_properties_callback)
endfunction()

setup()

run_test(test_get_target 1)
run_test(test_get_target_property 2)
run_test(test_narta_target_properties 7)
