cmake_minimum_required(VERSION 3.19)

include(${CMAKE_CURRENT_LIST_DIR}/Common.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Overrides.cmake)

set(_ignorable ARGV ARGV0 ARGV1 ARGV2
        _snapshot
        _callback
        _current_snapshot
        _prev_snapshot)

function(variable_add_callback _property)
    #log_info(test_variables "set(${_property} \"${_value}\")")
    if (ARGC EQUAL 2)
        if (NOT _property IN_LIST _ignorable)
            set(_value ${ARGV1})
            assertEqual(${_property} cool_variable)
            assertEqual("${_value}" cool_value)
            increment_invocation_count()
        endif ()
    endif()
endfunction()

function(variables_callback name value)
    set(_list CMAKE_BINARY_DIR;CMAKE_CURRENT_BINARY_DIR;CMAKE_CURRENT_LIST_DIR;CMAKE_CURRENT_LIST_FILE;
              CMAKE_CURRENT_SOURCE_DIR;CMAKE_VERSION)
    message(STATUS "${name} = ${value}")
    if (name IN_LIST _list)
        increment_invocation_count()
    endif()
endfunction()

function(test_get_variable)
    narta_get_variable(cool_variable variable_add_callback)
    set(cool_variable cool_value)
    narta_get_variable(cool_variable variable_add_callback)
endfunction()

function(test_variables)
    narta_variables(variables_callback)
endfunction()

run_test(test_get_variable 1)
run_test(test_variables 6)

