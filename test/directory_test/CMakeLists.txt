cmake_minimum_required(VERSION 3.19)
project(directory_test)

if (DIRECTORIES_TEST_ENABLED)
    include(${PROJECT_SOURCE_DIR}/../cmake/DirectoriesDeltaTest.cmake)
endif()

add_test(
        NAME directories_delta_test
        COMMAND ${CMAKE_COMMAND}
        -DDIRECTORIES_TEST_ENABLED=ON
        -Dnarta.path=${PROJECT_SOURCE_DIR}/../../cmake
        ../..
)
